/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufpr.tads.web2.facade;

import com.ufpr.tads.web2.beans.Cliente;
import com.ufpr.tads.web2.dao.ClienteDAO;
import java.util.List;

/**
 *
 * @author lucas
 */
public class ClientesFacade {
    
    public static void inserirCLiente(Cliente c){
        ClienteDAO dao = new ClienteDAO();
        dao.inserir(c);
    }
    
    public static void removerCliente(int id) {
        ClienteDAO dao = new ClienteDAO();
        dao.deletaId(id);
    }
    
    public static void updateCliente(Cliente c) {
        ClienteDAO dao = new ClienteDAO();
        dao.updateCliente(c);
    }
    
    public static Cliente buscarCliente(int id) {
        ClienteDAO dao = new ClienteDAO();
        return dao.buscarId(id);
    }
    
    public static List<Cliente> buscarTodosCli() {
        ClienteDAO dao = new ClienteDAO();
        return dao.buscarTodos();
    }
}
