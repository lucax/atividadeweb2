package com.ufpr.tads.web2.servlets;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

 /*
Obtém os dados passados pela PortalServlet, cria um objeto do tipo Usuario e insere os dados neste objeto. Após, apresenta uma tela contendo:
Uma mensagem que o usuário foi cadastrado com sucesso
Um link para PortalServlet
 */
import com.ufpr.tads.web2.beans.LoginBean;
import com.ufpr.tads.web2.dao.UsuarioDAO;
import com.ufpr.tads.web2.beans.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author lucas
 */
@WebServlet(urlPatterns = {"/CadastrarUsuarioServlet"})
public class CadastrarUsuarioServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        LoginBean user = (LoginBean) session.getAttribute("user");

        if (user == null) {
            RequestDispatcher rd = request.
                    getRequestDispatcher("erro.jsp");
            request.setAttribute("msg", "erro");
            request.setAttribute("page", "index.html");
            rd.forward(request, response);
        } 

        String nome = request.getParameter("nome");
        String login = request.getParameter("login");
        String senha = request.getParameter("senha");

        Usuario usuario = new Usuario();
        usuario.setNome(nome);
        usuario.setLogin(login);
        usuario.setSenha(senha);
        

        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CadastrarUsuarioServlet</title>");
            out.println("</head>");
            out.println("<body style=\"background-color: #efefef;\n" +
"                font-family: Tahoma, Arial, sans-serif;\n" +
"                text-align: center;\n" +
"                width: 100%;\n" +
"                height: 100vh;\n" +
"                margin: 0 auto;\n" +
"                border: 0;\n" +
"                position: relative;\n" +
"                display: flex;\n" +
"                flex-direction: row;\n" +
"                justify-content: center;\n" +
"                align-items: center;\">");
             out.println("<section style=\"width: 600px; min-height: 300px;background-color: #fff;padding: 50px;display: flex;flex-direction: column; box-shadow: 0 0 20px 5px #777;\">");
            try {
                UsuarioDAO dao = new UsuarioDAO();
                dao.inserir(usuario);
                out.println("<h1>Usuário cadastrado com sucesso</h1>");
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

               
            out.println("<h3>Nome </h3>" + nome);
            out.println("<br/><h3>Login </h3>" + login);
            out.println("<br/><h3>Senha </h3>" + senha);
            out.println("<div style=\" text-align: center; \n" +
"                border: none; \n" +
"                padding: 10px; \n" +
"                background-color: #A2C3DC; \n" +
"                width: 100px; \n" +
"                margin: 20px auto 30px auto;\n" +
"                box-shadow: 0 0 5px 1px #ccc;\">");
            out.println("<a style=\"text-decoration: none; color: #000000;\"href=\"/WebExer2ServletForm/portal.jsp\">voltar</a>");
            out.println("</div>");
             out.println("</section>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
