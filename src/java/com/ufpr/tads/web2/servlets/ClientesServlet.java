/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufpr.tads.web2.servlets;

import com.ufpr.tads.web2.beans.Cliente;
import com.ufpr.tads.web2.beans.Estado;
import com.ufpr.tads.web2.beans.LoginBean;
import com.ufpr.tads.web2.dao.ClienteDAO;
import com.ufpr.tads.web2.dao.EstadoDAO;
import com.ufpr.tads.web2.facade.ClientesFacade;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author lucas
 */
@WebServlet(name = "ClientesServlet", urlPatterns = {"/ClientesServlet"})
public class ClientesServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        HttpSession session = request.getSession();
        LoginBean user = (LoginBean) session.getAttribute("user");

        if (user == null) {
            RequestDispatcher rd = request.
                    getRequestDispatcher("index.jsp");
            request.setAttribute("msg", "Usuário deve se autenticar para acessar o sistema");
            rd.forward(request, response);
        } else {

            String action = request.getParameter("action");
            
            
            if (action.equals("list") || action == null) {

                List<Cliente> lista = ClientesFacade.buscarTodosCli();

                RequestDispatcher rd = request.
                        getRequestDispatcher("clientesListar.jsp");
                request.setAttribute("lista", lista);
                rd.forward(request, response);
            } else if (action.equals("show")) {
                String idString = request.getParameter("id");
                int id = Integer.parseInt(idString);

                Cliente c = ClientesFacade.buscarCliente(id);

                RequestDispatcher rd = request.
                        getRequestDispatcher("clientesVisualizar.jsp");
                request.setAttribute("cliente", c);
                rd.forward(request, response);
            } else if (action.equals("remove")) {
                String idString = request.getParameter("id");
                int id = Integer.parseInt(idString);

                ClientesFacade.removerCliente(id);

                RequestDispatcher rd = request.
                        getRequestDispatcher("ClientesServlet?action=list");
                rd.forward(request, response);
            } else if (action.equals("formUpdate")) {
                String idString = request.getParameter("id");
                int id = Integer.parseInt(idString);

                Cliente c = ClientesFacade.buscarCliente(id);

                EstadoDAO dao = new EstadoDAO();
                List<Estado> lista = dao.buscarTodos();

                RequestDispatcher rd = request.getRequestDispatcher("clientesForm.jsp");
                request.setAttribute("form", "alterar");
                request.setAttribute("lista", lista);
                request.setAttribute("cliente", c);
                rd.forward(request, response);
            } else if (action.equals("update")) {

                String idString = request.getParameter("id");
                int id = Integer.parseInt(idString);
                String nome = request.getParameter("nome");
                String cpf = request.getParameter("cpf");
                String email = request.getParameter("email");
                String dt = request.getParameter("data");
                Date data = null;
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                String rua = request.getParameter("rua");
                String nrString = request.getParameter("nr");
                int nr = Integer.parseInt(nrString);
                String cep = request.getParameter("cep");
                String cidade = request.getParameter("cidade");
                int idCidade = Integer.parseInt(cidade);
//                String uf = request.getParameter("uf");

                try {
                    data = (Date) format.parse(dt);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Cliente c = new Cliente();
                c.setId(id);
                c.setNome(nome);
                c.setCep(cep);
                c.setCpf(cpf);
                c.setEmail(email);
                c.setData(data);
                c.setNr(nr);
//                c.setCidade(cidade);
                c.setRua(rua);
//                c.setUf(uf);
                c.setIdCidade(idCidade);

                ClientesFacade.updateCliente(c);

                RequestDispatcher rd = request.getRequestDispatcher("ClientesServlet?action=list");
                rd.forward(request, response);

            } else if (action.equals("formNew")) {
                EstadoDAO dao = new EstadoDAO();
                List<Estado> lista = dao.buscarTodos();

                RequestDispatcher rd = request.getRequestDispatcher("clientesForm.jsp");
                request.setAttribute("lista", lista);
                rd.forward(request, response);
            } else if (action.equals("new")) {
                String nome = request.getParameter("nome");
                String cpf = request.getParameter("cpf");
                String email = request.getParameter("email");

                String dt = request.getParameter("dt");
                Date data = null;
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

                String rua = request.getParameter("rua");
                String nrString = request.getParameter("nr");
                int nr = Integer.parseInt(nrString);
                String cep = request.getParameter("cep");
                String cidade = request.getParameter("cidade");
                int id = Integer.parseInt(cidade);
//                String uf = request.getParameter("uf");

                try {
                    data = (Date) format.parse(dt);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Cliente c = new Cliente();
                c.setNome(nome);
                c.setCep(cep);
                c.setCpf(cpf);
                c.setEmail(email);
                c.setData(data);
                c.setNr(nr);
                c.setIdCidade(id);
//                c.setCidade(cidade);
                c.setRua(rua);
//                c.setUf(uf);

                ClientesFacade.inserirCLiente(c);

                RequestDispatcher rd = request.getRequestDispatcher("ClientesServlet?action=list");
                rd.forward(request, response);

            } else {
                RequestDispatcher rd = request.
                        getRequestDispatcher("erro.jsp");
                request.setAttribute("msg", "acao invalida");
                rd.forward(request, response);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
