/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufpr.tads.web2.dao;

import com.ufpr.tads.web2.beans.Cliente;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author lucas
 */
public class ClienteDAO {

    public ClienteDAO() {
    }

    public List<Cliente> buscarTodos() {
        List<Cliente> resultado = new ArrayList<>();
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;

        try {
            con = ConnectionFactory.getConnection();
            // tirei a data
//            st = con.prepareStatement("select id_cliente, cpf_cliente, nome_cliente, email_cliente, rua_cliente, nr_cliente, cep_cliente, data_cliente from tb_cliente");
            st = con.prepareStatement("SELECT c.id_cliente, c.cpf_cliente, c.nome_cliente, c.email_cliente, c.data_cliente,"
                    + " c.rua_cliente, c.nr_cliente, c.cep_cliente, cd.nome_cidade, e.nome_estado "
                    + "FROM tb_cliente c, tb_cidade cd, tb_estado e "
                    + "WHERE c.id_cidade=cd.id_cidade AND cd.id_estado=e.id_estado ");
            rs = st.executeQuery();
            while (rs.next()) {
                Cliente c = new Cliente();
                c.setId(rs.getInt("id_cliente"));
                c.setCpf(rs.getString("cpf_cliente"));
                c.setNome(rs.getString("nome_cliente"));
                c.setEmail(rs.getString("email_cliente"));
                c.setData(rs.getDate("data_cliente"));
                c.setRua(rs.getString("rua_cliente"));
                c.setNr(rs.getInt("nr_cliente"));
                c.setCep(rs.getString("cep_cliente"));
                c.setCidade(rs.getString("nome_cidade"));
                c.setUf(rs.getString("nome_estado"));

                resultado.add(c);
            }
            return resultado;
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                }
            }
        }
    }

    public void inserir(Cliente cliente) {
        Connection con = null;
        PreparedStatement st = null;

        try {
            con = ConnectionFactory.getConnection();
            st = con.prepareStatement("insert into tb_cliente"
                    + "(cpf_cliente, nome_cliente, email_cliente, data_cliente, rua_cliente, nr_cliente, cep_cliente, id_cidade)"
                    + " values (?, ?, ?, ?, ?, ?, ?, ?)");
            st.setString(1, cliente.getCpf());
            st.setString(2, cliente.getNome());
            st.setString(3, cliente.getEmail());
            st.setDate(4, new java.sql.Date(cliente.getData().getTime()));
            st.setString(5, cliente.getRua());
            st.setInt(6, cliente.getNr());
            st.setString(7, cliente.getCep());
            st.setInt(8, cliente.getIdCidade());

            st.executeUpdate();
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                }
            }
        }
    }

    public Cliente buscarId(Integer id) {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        try {
            con = ConnectionFactory.getConnection();
            st = con.prepareStatement("SELECT c.id_cliente, c.cpf_cliente, c.nome_cliente, c.email_cliente, c.data_cliente,"
                    + " c.rua_cliente, c.nr_cliente, c.cep_cliente, cd.nome_cidade, e.nome_estado "
                    + "FROM tb_cliente c, tb_cidade cd, tb_estado e "
                    + "WHERE c.id_cliente = ? AND c.id_cidade=cd.id_cidade AND cd.id_estado=e.id_estado ");
            st.setInt(1, id);
            rs = st.executeQuery();
            if (rs.next()) {
                Cliente c = new Cliente();
                c.setId(rs.getInt("id_cliente"));
                c.setCpf(rs.getString("cpf_cliente"));
                c.setNome(rs.getString("nome_cliente"));
                c.setEmail(rs.getString("email_cliente"));
                c.setData(rs.getDate("data_cliente"));
                c.setRua(rs.getString("rua_cliente"));
                c.setNr(rs.getInt("nr_cliente"));
                c.setCep(rs.getString("cep_cliente"));
                c.setCidade(rs.getString("nome_cidade"));
                c.setUf(rs.getString("nome_estado"));
                return c;
            } else {
                return null;
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                }
            }
        }
    }

    public void deletaId(Integer id) {
        Connection con = null;
        PreparedStatement st = null;

        try {
            con = ConnectionFactory.getConnection();
            st = con.prepareStatement("delete from tb_cliente where id_cliente = ?");
            st.setInt(1, id);
            st.executeUpdate();
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                }
            }
        }
    }

    public void updateCliente(Cliente cliente) {
        Connection con = null;
        PreparedStatement st = null;

        try {
            con = ConnectionFactory.getConnection();
            st = con.prepareStatement("UPDATE tb_cliente SET cpf_cliente = ?, nome_cliente = ?,"
                    +" email_cliente = ?, rua_cliente = ?, nr_cliente = ?, cep_cliente = ?, id_cidade = ? WHERE id_cliente = ?");
            st.setString(1, cliente.getCpf());
            st.setString(2, cliente.getNome());
            st.setString(3, cliente.getEmail());
//            st.setDate(4, cliente.getData());
            st.setString(4, cliente.getRua());
            st.setInt(5, cliente.getNr());
            st.setString(6, cliente.getCep());
            st.setInt(7, cliente.getIdCidade());
//            st.setString(7, cliente.getUf());
            st.setInt(8, cliente.getId());

            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                }
            }
        }
    }
}
