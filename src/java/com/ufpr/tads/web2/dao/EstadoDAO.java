/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufpr.tads.web2.dao;

import com.ufpr.tads.web2.beans.Estado;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lucas
 */
public class EstadoDAO {

    public EstadoDAO() {
    }

    public List<Estado> buscarTodos() {
        List<Estado> resultado = new ArrayList<>();
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;

        try {
            con = ConnectionFactory.getConnection();
            st = con.prepareStatement("SELECT id_estado, nome_estado, sigla_estado FROM tb_estado");
            rs = st.executeQuery();
            while (rs.next()) {
                Estado es = new Estado();
                es.setId(rs.getInt("id_estado"));
                es.setNome(rs.getString("nome_estado"));
                es.setSigla(rs.getString("sigla_estado"));
                resultado.add(es);
            }

            return resultado;
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                }
            }
        }
    }

}
