/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufpr.tads.web2.dao;

import com.ufpr.tads.web2.beans.Cidade;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lucas
 */
public class CidadeDAO {

    public CidadeDAO() {
    }

    public List<Cidade> buscarTodos(Integer id) {
        List<Cidade> resultado = new ArrayList<Cidade>();
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;

        try {

            con = ConnectionFactory.getConnection();
            st = con.prepareStatement("SELECT id_cidade, nome_cidade FROM tb_cidade WHERE id_estado = ?");
            st.setInt(1, id);
            rs = st.executeQuery();
            while (rs.next()) {
                Cidade c = new Cidade();
                c.setId(rs.getInt("id_cidade"));
                c.setNome(rs.getString("nome_cidade"));
                resultado.add(c);
            }

            return resultado;
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                }
            }
        }

    }
}
