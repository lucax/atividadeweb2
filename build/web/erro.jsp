<%-- 
    Document   : erro
    Created on : 12/09/2019, 14:34:54
    Author     : lucas
--%>
<%--<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>--%>
<%@ page isErrorPage="true" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>




<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="test/css/bootstrap.min.css">
        <!--<script src="bootstrap-4.3.1/js/bootstrap.min.js"></script>-->
        <script src="test/jquery-3.4.1.min.js"></script>
        <script src="test/jquery.mask.js"></script>
        <title>erro</title>
    </head>
    <body>

        <nav class="navbar navbar-dark bg-dark navbar-expand-sm">
            <a class="navbar-brand" href="#">
                Usuário ${user.nome}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-list-5" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbar-list-5">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="ClientesServlet?action=list">Cliente</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="LogoutServlet">Sair</a>
                    </li>
                </ul>
            </div>
        </nav>

        <!--mensagem-->
        ${pageContext.exception.message}

        <!--StackTrace-->
        ${pageContext.out.flush()}
        ${pageContext.exception.printStackTrace(pageContext.response.writer)}


        <!-- Rodapé da página -->
        <div class="container">
            <hr />
            <div class="text-center margin-bottom padding-bottom center-block">
                <h2>Em caso de problemas contactar o administrador</h2>
                E-mail admin: ${applicationScope.configuracao.email}
            </div>
            <hr />
        </div>

    </body>
</html>
