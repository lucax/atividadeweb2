<%-- 
    Document   : inserir
    Created on : 13/09/2019, 10:57:11
    Author     : lucas
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page errorPage="erro.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<c:if test="${empty sessionScope.user}" >
    <jsp:forward page="index.jsp" >
        <jsp:param name="msg" value="Usuário deve se autenticar para acessar o sistema" />
    </jsp:forward>
</c:if>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>inserir jsp</title>
    </head>
    <style>
        body {
            background-color: #efefef;
            font-family: Tahoma, Arial, sans-serif;
            text-align: center;
            width: 100%;
            height: 100vh;
            margin: 0 auto;
            border: 0;
            position: relative;
            display: flex;
            flex-direction: row;
            justify-content: center;
            align-items: center;
        }
        .box {
            width: 600px;
            min-height: 300px;
            background-color: #fff;
            padding: 50px;
            display: flex;
            flex-direction: column;
            box-shadow: 0 0 20px 5px #777;
        }
        h1 {
            font-size: 20px;
            margin-top: 0;
        }
        form {
            text-align: justify;
            display: flex;
            flex-direction: column;
            justify-content: center;
            width: 300px;
            margin: 0 auto;
        }
        form input {
            padding: 10px 0;
            margin: 5px 0;
        }
        .bot{
            text-align: center; 
            border: none; 
            padding: 10px; 
            background-color: #A2C3DC; 
            width: 100px; 
            margin: 20px auto 30px auto;
            box-shadow: 0 0 5px 1px #ccc;
        }
        #link {
            color: #000000; 
            text-decoration: none;
        }
    </style>
    <body>
    
        <section class="box">

            <h1>Novo Usuário</h1>

            <form action="http://localhost:8080/WebExer2ServletForm/CadastrarUsuarioServlet" method="POST">
                Nome: <input type="text" name="nome" /> <br/>
                Login: <input type="text" name="login"/> <br/>
                Senha: <input type="text" name="senha"/> <br/>

                <input class="bot" type="submit" value="criar" />

            </form>
        </section>
        <div class="item">
            E-mail admin: ${configuracao.email}
        </div>
    </body>
</html>
