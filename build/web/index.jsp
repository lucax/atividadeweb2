<%-- 
    Document   : index
    Created on : 22/09/2019, 13:18:12
    Author     : lucas
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page errorPage="erro.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>index jsp</title>
        <link rel="stylesheet" href="test/css/bootstrap.min.css">
        <!--<script src="bootstrap-4.3.1/js/bootstrap.min.js"></script>-->
        <script src="test/jquery-3.4.1.min.js"></script>
        <script src="test/jquery.mask.js"></script>

    </head>
    <body>

        <div class="container">
            <h2>Login</h2>
            <form action="LoginServlet">
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="text" class="form-control" id="email" placeholder="Enter email" name="login">
                </div>
                <div class="form-group">
                    <label for="pwd">Password:</label>
                    <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="senha">
                </div>
                <div class="form-group form-check">
                    <label class="form-check-label">
                        ${ !(empty param.msg) ? param.msg : msg}
                    </label>
                </div>
                <button type="submit" class="btn btn-primary">login</button>
            </form>
        </div>


        <div class="container">
            <hr />
            <div class="text-center margin-bottom padding-bottom center-block">
                <h2>Em caso de problemas contactar o administrador</h2>
                E-mail admin: ${applicationScope.configuracao.email}
            </div>
            <hr />
        </div>
    </body>
</html>
