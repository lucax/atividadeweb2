<%-- 
    Document   : clientesVisualizar
    Created on : 26/09/2019, 17:48:20
    Author     : lucas
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page errorPage="erro.jsp" %>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${empty sessionScope.user}" >
    <jsp:forward page="index.jsp">
        <jsp:param name="msg" value="Usuário deve se autenticar para acessar o sistema" />
    </jsp:forward>
</c:if>


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="test/css/bootstrap.min.css">
        <!--<script src="bootstrap-4.3.1/js/bootstrap.min.js"></script>-->

        <title>JSP Page</title>
    </head>
    <body>

        <nav class="navbar navbar-dark bg-dark navbar-expand-sm">
            <a class="navbar-brand" href="#">
                Hello <jsp:getProperty name="user" property="nome"/>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-list-5" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbar-list-5">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="ClientesServlet?action=list">Cliente</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="LogoutServlet">Sair</a>
                    </li>
                </ul>
            </div>
        </nav>

        <div class="jumbotron text-center">
            <h1>Dados do cliente</h1>
        </div>

        <section class="section">
            <div class="container">


                <ul class="list-group">
                    <li class="list-group-item">nome: ${cliente.nome}</li>
                    <li id="cpf" class="list-group-item">cpf: ${cliente.cpf}</li>
                    <li class="list-group-item">Data: <fmt:formatDate value="${cliente.data}" pattern="dd/MM/yyyy" /> <br /> </li>
                    <li class="list-group-item">email: ${cliente.email}</li>
                    <li class="list-group-item">rua: ${cliente.rua}</li>
                    <li id="cep" class="list-group-item">cep: ${cliente.cep}</li>
                    <li class="list-group-item">número: ${cliente.nr}</li>
                    <li class="list-group-item">cidade: ${cliente.cidade}</li>
                    <li class="list-group-item">UF: ${cliente.uf}</li>

                </ul>

            </div>
        </section>

        <section class="section">
            <div class="container">
                <div class="novo">
                    <a class="btn btn-dark btn-dark-cad" id="link" href="ClientesServlet?action=list">voltar</a>
                </div>
            </div>
        </section>

        <div class="container">
            <hr />
            <div class="text-center margin-bottom padding-bottom center-block">
                <h2>Em caso de problemas contactar o administrador</h2>
                E-mail admin: ${applicationScope.configuracao.email}
            </div>
            <hr />
        </div>
        <script src="test/jquery-3.4.1.min.js"></script>
        <script src="test/jquery.mask.js"></script>
        <script>
            $(document).ready(function () {
                $('.date').mask('00/00/0000');
                $('.time').mask('00:00:00');
                $('.date_time').mask('00/00/0000 00:00:00');
                $('.cep').mask('00000-000');
                $('.phone').mask('0000-0000');
                $('.phone_with_ddd').mask('(00) 0000-0000');
                $('.phone_us').mask('(000) 000-0000');
                $('.mixed').mask('AAA 000-S0S');
                $('#cpf').mask('000.000.000-00', {reverse: true});
                $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
                $('.money').mask('000.000.000.000.000,00', {reverse: true});
                $('.money2').mask("#.##0,00", {reverse: true});
                $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
                    translation: {
                        'Z': {
                            pattern: /[0-9]/, optional: true
                        }
                    }
                });
                $('.ip_address').mask('099.099.099.099');
                $('.percent').mask('##0,00%', {reverse: true});
                $('.clear-if-not-match').mask("00/00/0000", {clearIfNotMatch: true});
                $('.placeholder').mask("00/00/0000", {placeholder: "__/__/____"});
                $('.fallback').mask("00r00r0000", {
                    translation: {
                        'r': {
                            pattern: /[\/]/,
                            fallback: '/'
                        },
                        placeholder: "__/__/____"
                    }
                });
                $('.selectonfocus').mask("00/00/0000", {selectOnFocus: true});
            });
        </script>

    </body>
</html>
