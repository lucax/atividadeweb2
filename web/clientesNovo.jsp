<%-- 
    Document   : clientesNovo
    Created on : 26/09/2019, 15:02:08
    Author     : lucas
--%>

<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page errorPage="erro.jsp" %>
<%@ page contentType="text/html" pageEncoding="UTF-8"%>

<c:if test="${empty sessionScope.user}" >
    <jsp:forward page="index.jsp">
        <jsp:param name="msg" value="Usuário deve se autenticar para acessar o sistema" />
    </jsp:forward>
</c:if>

<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.min.css">
        <script src="bootstrap-4.3.1/js/bootstrap.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap-datepicker.css">
        <script src="bootstrap-4.3.1/js/bootstrap-datepicker.min.js"></script>

        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

        <title>JSP Page</title>
    </head>

    <body>

        <nav class="navbar navbar-dark bg-dark navbar-expand-sm">
            <a class="navbar-brand" href="#">
                Hello ${user.nome}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-list-5" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbar-list-5">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="ClientesServlet?action=list">Cliente</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="LogoutServlet">Sair</a>
                    </li>
                </ul>
            </div>
        </nav>

        <div class="jumbotron text-center">
            <h1>Novo Usuário</h1>
        </div>
        <%--<%= 0/0 %>--%>
        <div class="container">

            <form action="ClientesServlet?action=new" method="POST">
                <div class="form-group">

                    Nome <input class="form-control" placeholder="Nome" type="text" name="nome" /> <br/>
                    cpf <input class="form-control" placeholder="cpf" type="text" name="cpf"/> <br/>
                    email <input class="form-control" placeholder="email" type="text" name="email"/> <br/>

                    <div class="input-group date" data-provide="datepicker">
                        data <input id="datepicker" class="form-control" placeholder="dt" type="text" name="dt">
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                        </div>
                        <br/>
                    </div>

                    rua <input class="form-control" placeholder="rua" type="text" name="rua"/> <br/>
                    número <input class="form-control" placeholder="número" type="text" name="nr"/> <br/>
                    cep <input class="form-control" placeholder="cep" type="text" name="cep"/> <br/>
                    cidade <input class="form-control" placeholder="cidade" type="text" name="cidade"/> <br/>
                    <!--uf <input class="form-control" placeholder="UF" type="text" name="uf"/> <br/>-->
                    UF<select class="form-control" id="sel1" type="text" name="uf">
                        <c:forEach var="estado" items="${requestScope.lista}" >

                            <option>${estado.sigla}</option>


                        </c:forEach>
                    </select><br/>

                    <input class="btn btn-primary" type="submit" value="criar" />
                    <a id="link" class="btn btn-danger" href="ClientesServlet?action=list">Cancelar</a>
                </div>

            </form>
        </div>


        <div class="container">
            <hr />
            <div class="text-center margin-bottom padding-bottom center-block">
                <h2>Em caso de problemas contactar o administrador</h2>
                E-mail admin: ${applicationScope.configuracao.email}
            </div>
            <hr />
        </div>
        <script>
            $("#datepicker").datepicker({
                dateFormat: "dd-mm-yy"
            });
        </script>

    </body>
</html>
