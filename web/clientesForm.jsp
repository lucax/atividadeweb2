<%-- 
    Document   : clientesForm
    Created on : 01/11/2019, 12:21:09
    Author     : lucas
--%>

<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page errorPage="erro.jsp" %>

<c:if test="${empty sessionScope.user}" >
    <jsp:forward page="index.jsp">
        <jsp:param name="msg" value="Usuário deve se autenticar para acessar o sistema" />
    </jsp:forward>
</c:if>

<!DOCTYPE html>
<html>
    <head>
        <title>clientesForm</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="test/css/bootstrap.min.css">
        <link rel="stylesheet" href="test/css/bootstrap-datepicker.css">  <!-- Datepicker -->
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="/resources/demos/style.css">

    </head>
    <body>
        <nav class="navbar navbar-dark bg-dark navbar-expand-sm">
            <a class="navbar-brand" href="#">
                Hello ${user.nome}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-list-5" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbar-list-5">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="ClientesServlet?action=list">Cliente</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="LogoutServlet">Sair</a>
                    </li>
                </ul>
            </div>
        </nav>

        <div class="jumbotron text-center">
            <h1>${ requestScope.form == "alterar" ? "Alterar dados" : "Novo cliente"}</h1>
        </div>


        <div class="container">
            <form action="ClientesServlet?action=${ requestScope.form == "alterar" ? "update" : "new"}" method="POST">
                <div class="form-group">

                    <input style="display: none;"type="text" class="form-control" name="id" value="${requestScope.form == "alterar" ? cliente.id : ""}" /> <br/>

                    Nome <input type="text" class="form-control" name="nome" value="${requestScope.form == "alterar" ? cliente.nome : ""}" /> <br/>
                    cpf <input id="cpf" type="text" class="form-control" name="cpf" value="${requestScope.form == "alterar" ? cliente.cpf : ""}"/> <br/>
                    email <input type="email" class="form-control" name="email" value="${requestScope.form == "alterar" ? cliente.email : ""}" /> <br/>

                    <!--<div class="input-group date" data-provide="datepicker">-->
                    data <input id="datepicker" class="form-control" value="${requestScope.form == "alterar" ? cliente.data : ""}" placeholder="dt" type="text" name="dt">
                    <br/>
                    <!--</div>-->

                    rua <input type="text" class="form-control" name="rua" value="${requestScope.form == "alterar" ? cliente.rua : ""}"/> <br/>
                    número <input type="text" class="form-control" name="nr" value="${requestScope.form == "alterar" ? cliente.nr : ""}"/> <br/>
                    cep <input id="cep" type="text" class="form-control" name="cep" value="${requestScope.form == "alterar" ? cliente.cep : ""}"/> <br/>

                    UF <select class="form-control" id="estado" type="text" name="uf">
                        <c:forEach var="estado" items="${requestScope.lista}" >
                            <option value="${estado.id}">${estado.sigla}</option>
                        </c:forEach>
                    </select><br/>

                    cidade <select id="cidade" class="form-control" type="text" name="cidade">

                    </select><br/>

                    <input class="btn btn-primary" type="submit" value="${requestScope.form == "alterar" ? "alterar" : "novo"}" />
                    <a id="link" class="btn btn-danger" href="ClientesServlet?action=list">Cancelar</a>
                </div>
            </form>
        </div>

        <div class="container">
            <hr />
            <div class="text-center margin-bottom padding-bottom center-block">
                E-mail admin: ${applicationScope.configuracao.email}
            </div>
            <hr />
        </div>





        <script src="http://code.jquery.com/jquery-latest.js"></script>
        <script src="test/js/bootstrap.min.js"></script>
        <script src="test/js/jquery-3.4.1.min.js"></script>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="test/js/jquery.mask.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>
            $(document).ready(function () {
                $("#estado").change(function () {
                    getCidades();
                });

                $('.date').mask('00/00/0000');
                $('.time').mask('00:00:00');
                $('.date_time').mask('00/00/0000 00:00:00');
                $('#cep').mask('00000-000');
                $('.phone').mask('0000-0000');
                $('.phone_with_ddd').mask('(00) 0000-0000');
                $('.phone_us').mask('(000) 000-0000');
                $('.mixed').mask('AAA 000-S0S');
                $('#cpf').mask('000.000.000-00');
                $('.cnpj').mask('00.000.000/0000-00');
            });

            function getCidades() {
                var estadoId = $("#estado").val();
                var url = "AJAXServlet";
                $.ajax({
                    url: url, // URL da sua Servlet
                    data: {
                        estadoId: estadoId
                    }, // Parâmetro passado para a Servlet
                    dataType: 'json',
                    success: function (data) {
                        // Se sucesso, limpa e preenche a combo de cidade
                        // alert(JSON.stringify(data));
                        $("#cidade").empty();
                        $.each(data, function (i, obj) {
                            $("#cidade").append('<option value=' + obj.id + '>' + obj.nome + '</option>');
                        });
                    },
                    error: function (request, textStatus, errorThrown) {
                        alert(request.status + ', Error: ' + request.statusText);
                        // Erro
                    }
                });
            }

            $("#datepicker").datepicker({
                dateFormat: "dd/mm/yy"
            });

        </script>

    </body>
</html>
