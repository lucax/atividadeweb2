<%-- 
    Document   : portal
    Created on : 12/09/2019, 14:45:15
    Author     : lucas
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page errorPage="erro.jsp" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<c:if test="${empty sessionScope.user}" >
    <jsp:forward page="index.jsp">
        <jsp:param name="msg" value="Usuário deve se autenticar para acessar o sistema" />
    </jsp:forward>
</c:if>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>portal jsp</title>
        <link rel="stylesheet" href="test/css/bootstrap.min.css">
        <!--<script src="bootstrap-4.3.1/js/bootstrap.min.js"></script>-->
        <script src="test/jquery-3.4.1.min.js"></script>
        <script src="test/jquery.mask.js"></script>

    </head>

    <body>

        <nav class="navbar navbar-dark bg-dark navbar-expand-sm">
            <a class="navbar-brand" href="#">
                Hello ${user.nome}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-list-5" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbar-list-5">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="ClientesServlet?action=list">Cliente</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="LogoutServlet">Sair</a>
                    </li>
                </ul>
            </div>
        </nav>


        <div class="container">
            <hr />
            <div class="text-center margin-bottom padding-bottom center-block">
                <h2>Em caso de problemas contactar o administrador</h2>
                E-mail admin: ${applicationScope.configuracao.email}
            </div>
            <hr />
        </div>


    </body>
</html>
