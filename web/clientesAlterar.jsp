<%-- 
    Document   : clientesAlterar
    Created on : 22/09/2019, 14:22:25
    Author     : lucas
--%>

<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page errorPage="erro.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<c:if test="${empty sessionScope.user}" >
    <jsp:forward page="index.jsp" >
        <jsp:param name="msg" value="Usuário deve se autenticar para acessar o sistema"/>
    </jsp:forward>
</c:if>

<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="bootstrap-4.3.1-dist/css/bootstrap.min.css">
        <script src="bootstrap-4.3.1/js/bootstrap.min.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>Alterar Cliente</title>
    </head>
    <body>

        <nav class="navbar navbar-dark bg-dark navbar-expand-sm">
            <a class="navbar-brand" href="#">
                Hello ${user.nome}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-list-5" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbar-list-5">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="ClientesServlet?action=list">Cliente</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="LogoutServlet">Sair</a>
                    </li>
                </ul>
            </div>
        </nav>

        <div class="jumbotron text-center">
            <h1>Alterar Usuário</h1>
        </div>

        <div class="container">
            <form action="ClientesServlet?action=update" method="POST">
                <div class="form-group">

                    <input style="display: none;"type="text" class="form-control" name="id" value="${cliente.id}" /> <br/>


                    Nome <input type="text" class="form-control" name="nome" value="${cliente.nome}" /> <br/>
                    cpf <input type="text" class="form-control" name="cpf" value="${cliente.cpf}"/> <br/>
                    email <input type="text" class="form-control" name="email" value="${cliente.email}" /> <br/>
                    data: <input type="text" name="data" value="${cliente.data}"> <br/>
                    rua <input type="text" class="form-control" name="rua" value="${cliente.rua}"/> <br/>
                    número <input type="text" class="form-control" name="nr" value="${cliente.nr}"/> <br/>
                    cep <input type="text" class="form-control" name="cep" value="${cliente.cep}"/> <br/>
                    cidade <input type="text" class="form-control" name="cidade" value="${cliente.cidade}" /> <br/>             
                    UF<select class="form-control" id="sel1" type="text" name="uf">
                        <c:forEach var="estado" items="${requestScope.lista}" >

                            <option>${estado.sigla}</option>


                        </c:forEach>
                    </select><br/>

                    <input class="btn btn-primary" type="submit" value="Alterar" />
                    <a id="link" class="btn btn-danger" href="ClientesServlet?action=list">Cancelar</a>
                </div>
            </form>
        </div>

        <div class="container">
            <hr />
            <div class="text-center margin-bottom padding-bottom center-block">
                E-mail admin: ${applicationScope.configuracao.email}
            </div>
            <hr />
        </div>
    </body>
</html>
