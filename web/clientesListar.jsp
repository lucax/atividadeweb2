<%-- 
    Document   : clientesListar
    Created on : 22/09/2019, 14:20:03
    Author     : lucas
--%>

<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page errorPage="erro.jsp" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<c:if test="${empty sessionScope.user}" >
    <jsp:forward page="index.jsp" >
        <jsp:param name="msg" value="Usuário deve se autenticar para acessar o sistema" />
    </jsp:forward>
</c:if>


<!DOCTYPE html>
<html>
    <head>
        <title>JSP Page</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <link rel="stylesheet" href="test/css/bootstrap.min.css">
        <!--<script src="bootstrap-4.3.1/js/bootstrap.min.js"></script>-->

    </head>
    <body>

        <nav class="navbar navbar-dark bg-dark navbar-expand-sm">
            <a class="navbar-brand" href="#">
                Hello ${user.nome}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-list-5" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbar-list-5">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="ClientesServlet?action=list">Cliente</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="LogoutServlet">Sair</a>
                    </li>
                </ul>
            </div>
        </nav>


        <div class="jumbotron text-center">
            <h1>Lista de clientes</h1>
        </div>

        <div class="container">
            <table class="table" style="width: 100%">
                <thead class="thead-dark">
                    <tr>
                        <th>nome</th>
                        <th>cpf</th>
                        <th>email</th>
                        <th>Ações<th>
                    </tr>
                </thead>
                <c:forEach var="cliente" items="${requestScope.lista}" >
                    <tr>
                        <td>${cliente.nome}</td>
                        <td>${cliente.cpf}</td>
                        <td>${cliente.email}</td>
                        <td>
                            <a id="btn-remover" class="btn btn-danger btn-xs" href="ClientesServlet?action=remove&id=${cliente.id}">remover</a>
                            <a id="btn-visualizar" class="btn btn-success btn-xs" href="ClientesServlet?action=show&id=${cliente.id}">visualizar</a>
                            <a id="btn-alterar" class="btn btn-warning btn-xs" href="ClientesServlet?action=formUpdate&id=${cliente.id}">alterar</a>
                        </td>
                    </tr>
                </c:forEach>
            </table>
            <section class="section">
                <div class="container">
                    <div class="novo">
                        <a class="btn btn-dark btn-dark-cad" id="link" href="ClientesServlet?action=formNew">Cadastrar</a>
                    </div>
                </div>
            </section>
        </div>

        <div class="container">
            <hr />
            <div class="text-center margin-bottom padding-bottom center-block">
                <h2>Em caso de problemas contactar o administrador</h2>
                E-mail admin: ${applicationScope.configuracao.email}
            </div>
            <hr />
        </div>
        <!--        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>-->
        
        <script src="test/jquery-3.4.1.min.js"></script>
        <script src="test/jquery.mask.js"></script>
        <script>
            $(function () {
                $("#btn-remover").on("click", function () {
                    return confirm("Você tem certeza que deseja remover esse cliente?");
                });
            })
        </script>
    </body>
</html>
